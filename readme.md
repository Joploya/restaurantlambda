# Projet restaurant

Projet implémenté à partir d'un fichier PSD comme modèle.

Ce site a été construit avec [Bootstrap 4](https://getbootstrap.com/)

Dans le formulaire, le champ date, si on utilise directement le 'type=date' on rencontre un problème de compatibilité avec certains navigateurs. Pour corriger cela on peut utiliser date polyfill. Ici on utilise celui fournit par [le repository de chemerisuk](https://github.com/chemerisuk/better-dateinput-polyfill).

